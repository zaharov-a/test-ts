define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class HttpService {
        constructor() {
            this._defaultRequestInit = {
                credentials: 'include',
                dataType: 'json',
            };
        }
        get(url, data = null, params = null) {
            return this.request(HttpService.METHOD_GET, url, data, params);
        }
        post(url, data = null, params = null) {
            return this.request(HttpService.METHOD_POST, url, data, params);
        }
        request(method, url, data = null, params = null) {
            const requestInit = {
                method: method,
            };
            for (const [key, value] of Object.entries(this._defaultRequestInit)) {
                requestInit[key] = value;
            }
            if (params) {
                for (const [key, value] of Object.entries(params)) {
                    requestInit[key] = value;
                }
            }
            return fetch(url, requestInit)
                .then(response => {
                if (response.status == 200) {
                    // если необходимо разные типы
                    switch (params.dataType) {
                        case HttpService.DATATYPE_HTML:
                            return response.text();
                        case HttpService.DATATYPE_JSON:
                            return response.json();
                        default:
                            throw Error('unknown dataType');
                    }
                }
                throw Error(`code ${response.status}`);
            });
        }
    }
    HttpService.METHOD_GET = 'GET';
    HttpService.METHOD_POST = 'POST';
    // если необходимы разные типы
    HttpService.DATATYPE_JSON = 'json';
    HttpService.DATATYPE_HTML = 'html';
    exports.default = HttpService;
});
