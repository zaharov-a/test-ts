define(["require", "exports", "./http.service"], function (require, exports, http_service_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class AppService {
        constructor(_httpService) {
            this._httpService = _httpService;
        }
        get httpService() {
            return this._httpService;
        }
    }
    const appService = new AppService(new http_service_1.default());
    exports.default = appService;
});
