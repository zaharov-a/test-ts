define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Model {
        constructor(_indexService) {
            this._indexService = _indexService;
        }
        load() {
            this._indexService.getTestData()
                .then(data => {
                this._data = data;
                this._error = null;
                this._observer();
            })
                .catch((error) => {
                this._error = error.message;
                this._data = null;
                this._observer();
            });
        }
        subscribe(cb) {
            this._observer = cb;
        }
        get data() {
            return this._data;
        }
        get error() {
            return this._error;
        }
    }
    exports.Model = Model;
});
