define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class View {
        constructor(_parent) {
            this._parent = _parent;
            this._base = 'view';
        }
        init() {
            // this._stdout = document.createElement('pre');
            // this._btn    = document.createElement('input');
            //
            // this._btn.type = 'button';
            // this._btn.value = 'обновить';
            //
            // this._parent.appendChild(this._btn);
            // this._parent.appendChild(this._stdout);
            this._parent.innerHTML = `
<input type="button" class="${this.getClass('btn')}" value="Обновить">
<pre class="${this.getClass('stdout')}"></pre>
`;
            this._stdout = this.getElement('stdout');
            this._btn = this.getElement('btn');
        }
        update(model) {
            if (model.error) {
                this._stdout.innerText = model.error;
            }
            else {
                this._stdout.innerText = JSON.stringify(model.data, undefined, 2);
            }
        }
        onBtnClick(cb) {
            this._btn.onclick = cb;
        }
        getElement(name) {
            return this._parent.querySelector('.' + this.getClass(name));
        }
        getClass(name) {
            return `${this._base}__${name}`;
        }
    }
    exports.View = View;
});
