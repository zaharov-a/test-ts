define(["require", "exports", "./service/index.service", "../service/app.service", "./model", "./view", "./controller"], function (require, exports, index_service_1, app_service_1, model_1, view_1, controller_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    // import AmdSelect from "../material/amd-select";
    // import AmdSelect from "../material/amd-select";
    const indexService = new index_service_1.default(app_service_1.default.httpService);
    const model = new model_1.Model(indexService);
    const view = new view_1.View(document.querySelector('div'));
    const controller = new controller_1.Controller(view, model);
    controller.init();
});
// customElements.define('amd-select', AmdSelect);
// indexService.getTestData()
//     .then((data: ITestData) =>
//     {
//         console.log('success', data);
//     })
//     .catch(data =>
//     {
//         console.log('fail', data);
//     });
//
// indexService.getTestBadData()
//     .then(data =>
//     {
//         console.log('success', data);
//     })
//     .catch(data =>
//     {
//         console.log('fail', data);
//     });
