define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Controller {
        constructor(_view, _model) {
            this._view = _view;
            this._model = _model;
        }
        init() {
            this._view.init();
            this._view.onBtnClick(() => {
                this.load();
            });
            this._model.subscribe(() => {
                this.update();
            });
            this.load();
        }
        load() {
            this._model.load();
        }
        update() {
            this._view.update(this._model);
        }
    }
    exports.Controller = Controller;
});
