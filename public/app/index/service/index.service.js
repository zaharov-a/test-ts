define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class IndexService {
        constructor(_httpService) {
            this._httpService = _httpService;
        }
        getTestData() {
            const url = Math.round(Math.random()) ? 'data/test.json' : 'data/testb.json';
            return this._httpService.get(url);
        }
    }
    exports.default = IndexService;
});
