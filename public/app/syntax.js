define(["require", "exports", "./service/http.service"], function (require, exports, http_service_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    {
        let a; // строка
        let b; // число
        let c; // массив строк
        let d; // массив строк
        let e; // говно
    }
    // переменные c и d равнозначны
    // тип any обозначает что угодно, тип по умолчанию
    // так же можно указать несколько типов
    {
        let a;
    }
    // можно указать в качестве типа возможные значения
    {
        let a;
        let b;
        a = ''; // предупреждение
        a = 'get';
        b = 7; // предупреждение
        b = 6;
    }
    //
    // Сложные типы:
    {
        let a; // объект со свойствами id типа number и name типа string
        let b; // массив объектов типа переменой а
        let c; // массив объектов типа переменой а
        let d; // объект, у которго ключи и значения типа string, ассоциативный массив грубо говоря
    }
    // тогда объявление будет выглядить как:
    {
        let a; // объект со свойствами id типа number и name типа string
        let b; // массив объектов типа переменой а
        let c; // массив объектов типа переменой а
        let d; // объект, у которго ключи и значения типа string, ассоциативный массив грубо говоря
    }
    // тогда возможны объявления
    {
        let a;
        let b;
    }
    // так же обощения можно использовать и в классах, допустим в нашем гриде
    class FilterGrid {
        onClick(index, row, rowData, e) {
        }
    }
    {
        const grid = new FilterGrid();
        // тут будет работать автоподстановка, и проверка наличия поля
        grid.lastSelectionData[0].id;
    }
    // Так же обобщения можно использовать и в возвращаемых значениях функций
    {
        const httpService = new http_service_1.default();
        httpService.get('/url').then(data => {
            // тут будет работать автоподстановка, и проверка наличия поля
            data.id;
        });
    }
    // Приведения типов
    // можно явно уточнить тип, допустим если тип any
    {
        let a;
        let b = a;
        let c = a;
    }
    // переменные b и c имеют одинаковый тип, string
    // Второй вариант уточнить наследуемый тип
    // Есть базвый класс Element
    // , от которого наследуется HTMLElement
    // , от которого появляется HTMLSelectElement
    // У класса Element нет свойства options, но оно есть у HTMLSelectElement
    {
        let a = document.querySelector('.input'); // Тип по умолчанию HTMLElement
        let b = document.querySelector('.select');
        a.options; // предупреждение
        b.options; // все ок
    }
    //Классы
    // объявление класса
    {
        class A {
        }
        class B extends A {
        }
    }
    // параметры конструктора
    // если при описании конструктора указать область видимости параметра,
    // то это автоматически создаст свойство и определит его
    {
        class A {
            constructor(a, b) {
                this.a = a;
                this.b = b;
            }
        }
    }
    {
        class A {
            constructor(a, b) {
                this.a = a;
                this.b = b;
            }
        }
    }
    // результат одинаковый
    // Вызов родительских методов
    {
        class A {
            constructor(a) {
                this.a = a;
            }
            method() {
                return this.a * 2;
            }
        }
        class B extends A {
            constructor(a) {
                super(a);
                this.a = a;
            }
            method() {
                return super.method();
            }
        }
    }
    // перезагрузка методов
    // взависимости от типов параметров, может меняться тип результата
    // значения по умолчанию  устанавливаюся только при первом объявлении
    {
        function f1(a, b) {
        }
        function f4(a, b = '1') {
        }
    }
    // неопределенное кол-во аргументов
    {
        function f(...args) {
        }
    }
    class A {
    }
    exports.A = A;
    exports.b = 3;
    var d = 4;
    exports.default = d;
    // Импрот (phpstorm делает сам)
    // import HttpService from "./service/http.service"; // HttpService определен по умолчанию
    // import {TTestData} from "./index/service/index.service"; // ITestData обычный export
    // для работы необходимо подключить requirejs
    // <script data-main="app/index/index" src="vendor/require.js"></script>
    // data-main="app/index/index" путь до начального скрипта
    // динамический импорт
    {
        let path = "";
        new Promise((resolve_1, reject_1) => { require([path], resolve_1, reject_1); }).then((data) => // typeof import("./service/http.service") - тип структура из файла
         {
            // data имеет структуру испортируемого файла
            data.default; //экспорт по умолчанию
            data.ClassName; // какой-то класс из файла
        })
            .catch((reason) => {
            console.log(reason);
        });
    }
    // Интерфейсы
    // интерфейсы можно использовать как для типов, так и как обычно
    {
        class RClass {
            get() {
                return "";
            }
            set(value) {
                return undefined;
            }
        }
    }
    // Необязательне параметры и значения по умолчанию
    {
        // значение по умолчанию указывается через =
        function f2(param = 0) {
        }
        f2();
        // необязательный параметр указывается с ?
        // в функциях и методах класса
        function f3(param) {
        }
        f3();
        let a = {}; // предупреждение
        let b = { field1: '' }; // ок
    }
    // Уточнение this в функциях
    {
        let a = $('input');
        a.click(function () {
            this.value; // неопределенность
        });
        a.click(function () {
            this.value; // ok
        });
    }
});
