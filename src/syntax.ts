// Синтаксис
// https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html
// https://www.typescriptlang.org/docs/handbook/basic-types.html
// https://www.typescriptlang.org/docs/home.html
// Все что есть сейчас в JS + будущие конструкции + типизация
//
// Указание типа
// Тип указывается через двоеточие, например:
// Простые типы
import HttpService from "./service/http.service";

{
    let a: string; // строка
    let b: number; // число
    let c: Array<string>; // массив строк
    let d: string[]; // массив строк
    let e: any; // говно
}
// переменные c и d равнозначны
// тип any обозначает что угодно, тип по умолчанию
// так же можно указать несколько типов
{
    let a: string | number;
}
// можно указать в качестве типа возможные значения
{
    let a: 'get' | 'post';
    let b: 5 | 6;

    a = ''; // предупреждение
    a = 'get';

    b = 7; // предупреждение
    b = 6;
}
//
// Сложные типы:
{
    let a: { id: number, name: string }; // объект со свойствами id типа number и name типа string
    let b: Array<{ id: number, name: string }>; // массив объектов типа переменой а
    let c: { id: number, name: string }[]; // массив объектов типа переменой а
    let d: { [key: string]: string }; // объект, у которго ключи и значения типа string, ассоциативный массив грубо говоря
}


// Сложные типы можно определить через интерфейс:
interface TA
{
    id: number;
    name: string;
}


interface AssocString
{
    [key: string]: string
}


// тогда объявление будет выглядить как:
{
    let a: TA; // объект со свойствами id типа number и name типа string
    let b: Array<TA>; // массив объектов типа переменой а
    let c: TA[]; // массив объектов типа переменой а
    let d: AssocString; // объект, у которго ключи и значения типа string, ассоциативный массив грубо говоря
}
//Обобщения, дженерики
// Чтобы не писать для каждого типа значений свой ассоциативный тип можно использовать обощения
interface Assoc<T>
{
    [key: string]: T;
}


// тогда возможны объявления
{
    let a: Assoc<string>;
    let b: Assoc<TA>;
}


// так же обощения можно использовать и в классах, допустим в нашем гриде
class FilterGrid<T>
{
    public lastSelectionData: Array<T>;


    public onClick(index: number, row, rowData: T, e: Event)
    {

    }
}


{
    const grid = new FilterGrid<TA>();
    // тут будет работать автоподстановка, и проверка наличия поля
    grid.lastSelectionData[0].id;
}

// Так же обобщения можно использовать и в возвращаемых значениях функций
{
    const httpService = new HttpService();

    httpService.get<TA>('/url').then(data =>
    {
        // тут будет работать автоподстановка, и проверка наличия поля
        data.id;
    });
}

// Приведения типов
// можно явно уточнить тип, допустим если тип any
{
    let a: any;
    let b = a as string;
    let c = <string>a;
}
// переменные b и c имеют одинаковый тип, string

// Второй вариант уточнить наследуемый тип
// Есть базвый класс Element
// , от которого наследуется HTMLElement
// , от которого появляется HTMLSelectElement
// У класса Element нет свойства options, но оно есть у HTMLSelectElement
{
    let a = document.querySelector('.input'); // Тип по умолчанию HTMLElement
    let b = document.querySelector('.select') as HTMLSelectElement;
    a.options; // предупреждение
    b.options; // все ок
}

//Классы
// объявление класса
{
    class A
    {

    }


    class B extends A
    {

    }
}
// параметры конструктора
// если при описании конструктора указать область видимости параметра,
// то это автоматически создаст свойство и определит его
{
    class A
    {
        public a: string;
        public b: string;


        constructor(a: string, b: string)
        {
            this.a = a;
            this.b = b;
        }
    }
}

{
    class A
    {
        constructor(public a: string, public b: string)
        {
        }
    }
}
// результат одинаковый

// Вызов родительских методов
{
    class A
    {

        constructor(protected a: number)
        {

        }


        method()
        {
            return this.a * 2;
        }
    }


    class B extends A
    {
        constructor(protected a: number)
        {
            super(a);
        }


        method(): number
        {
            return super.method();
        }
    }
}

// перезагрузка методов
// взависимости от типов параметров, может меняться тип результата
// значения по умолчанию  устанавливаюся только при первом объявлении
{
    function f1(a: string, b: string): string;
    function f1(a: number, b: number): number;
    function f1(a: any, b: any): any
    {

    }


    function f4(a: string, b: number): number;
    function f4(a: number, b: string): string;
    function f4(a: any, b: any = '1'): any
    {

    }
}

// неопределенное кол-во аргументов
{
    function f(...args: string[])
    {

    }
}

// Модули
// Используемая реализация requirejs (AMD)

// все что может быть использовано за пределами файла отмечается словом export
// export default - значение по умолчанию, только одно на файл
export interface TAE extends TA
{

}


export class A
{

}


export var b = 3;

var d = 4;
export default d;

// Импрот (phpstorm делает сам)
// import HttpService from "./service/http.service"; // HttpService определен по умолчанию
// import {TTestData} from "./index/service/index.service"; // ITestData обычный export

// для работы необходимо подключить requirejs
// <script data-main="app/index/index" src="vendor/require.js"></script>
// data-main="app/index/index" путь до начального скрипта

// динамический импорт
{
    let path: string = "";
    import(path)
        .then((data: typeof import("./service/http.service")) => // typeof import("./service/http.service") - тип структура из файла
        {
            // data имеет структуру испортируемого файла
            data.default; //экспорт по умолчанию
            data.ClassName; // какой-то класс из файла
        })
        .catch((reason) =>
        {
            console.log(reason);
        });
}

// Интерфейсы
// интерфейсы можно использовать как для типов, так и как обычно
{
    interface IClass
    {
        get(): string;


        set(value: string): IClass;
    }


    class RClass implements IClass
    {
        get(): string
        {
            return "";
        }


        set(value: string): IClass
        {
            return undefined;
        }

    }
}

// Необязательне параметры и значения по умолчанию
{
    // значение по умолчанию указывается через =
    function f2(param: number = 0)
    {

    }


    f2();

    // необязательный параметр указывается с ?
    // в функциях и методах класса
    function f3(param?: string)
    {

    }


    f3();


    // в интерфейсах
    interface TType
    {
        field1: string;
        field2?: string;

    }


    let a: TType = {}; // предупреждение
    let b: TType = {field1: ''}; // ок
}

// Объявления
// Можно описать стурктуру написанную на js
// пример в файлах *.d.ts
declare var da: string;


declare class DA
{
    get(): string;


    set(value: string): string;
}


// Уточнение this в функциях
{
    let a = $('input');

    a.click(function ()
    {
        this.value; // неопределенность
    });

    a.click(function (this: HTMLInputElement)
    {
        this.value; // ok
    });
}