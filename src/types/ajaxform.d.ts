interface AjaxFormParams
{
    width: number,
    style: string, //normal, strict
    hideOnEscape?: boolean,
    postBuildCallback?: (form: AjaxForm) => any,
    useFormData?: boolean,
    destroyOnHide?: boolean
}
interface AjaxFormAjaxParams
{
    url: string,
    data?: any,
    async?: boolean,
    type?: 'GET' | 'POST',
    dataType: 'json' | 'JSON' | 'xml' | 'script' | 'html',
    success?: (data: any) => void,
}
interface AjaxFormAjaxOptions
{
    complete?: () => any,
    done?: (data: any) => void,
    fail?: () => any,
    error?: (data: any) => void,
}
declare class AjaxForm
{
    constructor(dom: JQuery | string, params: AjaxFormParams);
    show(): void;
    hide(): void;
    getDomObject(): JQuery;
    getFormDomObject(): JQuery;
    ajaxStatus(status: boolean): void;
    done(callback: (data: any, form: AjaxForm) => any): void;
    ajax(
        ajaxParams: AjaxFormAjaxParams,
        options: AjaxFormAjaxOptions
    ): void;
    validation: {
        append: (callback: (form: AjaxForm) => any) => void
    };
    setShowCallback(callback: () => any): void;
    setHideCallback(callback: () => any): void;
    submit(callback?: () => any): void;
    validate(): void;
}