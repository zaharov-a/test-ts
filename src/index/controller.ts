import {View} from "./view";
import {Model} from "./model";


export class Controller
{

    public constructor(protected _view: View, protected _model: Model)
    {

    }


    public init()
    {
        this._view.init();

        this._view.onBtnClick(() =>
        {
            this.load();
        });

        this._model.subscribe(() =>
        {
            this.update();
        });

        this.load();
    }


    public load()
    {
        this._model.load();
    }


    public update()
    {
        this._view.update(this._model);
    }
}