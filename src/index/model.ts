import IndexService, {TTestData} from "./service/index.service";


export class Model
{

    private _data: TTestData;
    protected _observer;
    private _error;


    public constructor(protected _indexService: IndexService)
    {

    }


    public load()
    {
        this._indexService.getTestData()
            .then(data =>
            {
                this._data  = data;
                this._error = null;
                this._observer();
            })
            .catch((error: Error) =>
            {
                this._error = error.message;
                this._data  = null;
                this._observer();
            });
    }


    public subscribe(cb: () => void)
    {
        this._observer = cb;
    }


    get data(): TTestData
    {
        return this._data;
    }


    get error()
    {
        return this._error;
    }
}