import {Model} from "./model";


export class View
{
    protected _stdout: HTMLPreElement;
    protected _btn: HTMLInputElement;
    protected _base = 'view';


    public constructor(protected _parent: HTMLElement)
    {

    }


    public init()
    {
        // this._stdout = document.createElement('pre');
        // this._btn    = document.createElement('input');
        //
        // this._btn.type = 'button';
        // this._btn.value = 'обновить';
        //
        // this._parent.appendChild(this._btn);
        // this._parent.appendChild(this._stdout);

        this._parent.innerHTML = `
<input type="button" class="${this.getClass('btn')}" value="Обновить">
<pre class="${this.getClass('stdout')}"></pre>
`;
        this._stdout           = <HTMLPreElement>this.getElement('stdout');
        this._btn              = this.getElement('btn') as HTMLInputElement;

    }


    public update(model: Model)
    {
        if (model.error) {
            this._stdout.innerText = model.error;
        } else {
            this._stdout.innerText = JSON.stringify(model.data, undefined, 2);
        }
    }


    public onBtnClick(cb: () => void)
    {
        this._btn.onclick = cb;
    }


    public getElement(name)
    {
        return this._parent.querySelector('.' + this.getClass(name));
    }


    public getClass(name)
    {
        return `${this._base}__${name}`;
    }
}