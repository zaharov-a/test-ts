import HttpService from "../../service/http.service";


interface TTestDataRow
{
    id: number;
    title: string;
}


export interface TTestData
{
    success: boolean;
    data: TTestDataRow[];
}


export default class IndexService
{
    public constructor(protected _httpService: HttpService)
    {

    }


    getTestData(): Promise<TTestData>
    {
        const url = Math.round(Math.random()) ? 'data/test.json' : 'data/testb.json';
        return this._httpService.get<TTestData>(url);
    }

}