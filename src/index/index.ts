import IndexService, {TTestData} from "./service/index.service";
import appService from "../service/app.service";
import {Model} from "./model";
import {View} from "./view";
import {Controller} from "./controller";
// import AmdSelect from "../material/amd-select";
// import AmdSelect from "../material/amd-select";

const indexService = new IndexService(appService.httpService);
const model        = new Model(indexService);
const view         = new View(document.querySelector('div'));
const controller   = new Controller(view, model);

controller.init();

// customElements.define('amd-select', AmdSelect);

// indexService.getTestData()
//     .then((data: ITestData) =>
//     {
//         console.log('success', data);
//     })
//     .catch(data =>
//     {
//         console.log('fail', data);
//     });
//
// indexService.getTestBadData()
//     .then(data =>
//     {
//         console.log('success', data);
//     })
//     .catch(data =>
//     {
//         console.log('fail', data);
//     });