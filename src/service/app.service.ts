import HttpService from "./http.service";


class AppService
{
    constructor(protected _httpService: HttpService)
    {

    }


    get httpService(): HttpService
    {
        return this._httpService;
    }
}


const appService = new AppService(new HttpService());

export default appService;
