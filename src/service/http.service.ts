interface IHttpServiceParams extends RequestInit
{
    dataType: string; // если необходимы разные типы
}


interface IHttpServiceParamsHtml extends IHttpServiceParams
{
    dataType: 'html'; // если необходимы разные типы
}


interface IHttpServiceParamsJson extends IHttpServiceParams
{
    dataType: 'json'; // если необходимы разные типы
}


export default class HttpService
{
    public static readonly METHOD_GET  = 'GET';
    public static readonly METHOD_POST = 'POST';

    // если необходимы разные типы
    public static readonly DATATYPE_JSON = 'json';
    public static readonly DATATYPE_HTML = 'html';

    protected _defaultRequestInit: IHttpServiceParams = {
        credentials: 'include', // для передачи авторизации
        dataType   : 'json', // тип по умолчанию
    };


    public get<T>(url: string, data?, params?: IHttpServiceParamsJson): Promise<T>;
    public get<T>(url: string, data?, params?: IHttpServiceParamsHtml): Promise<string>;
    public get<T>(url: string, data = null, params: IHttpServiceParams = null): Promise<any>
    {
        return this.request<T>(HttpService.METHOD_GET, url, data, params);
    }


    public post<T>(url: string, data?, params?: IHttpServiceParamsJson): Promise<T> | Promise<T>;
    public post<T>(url: string, data?, params?: IHttpServiceParamsHtml): Promise<T> | Promise<string>;
    public post<T>(url: string, data = null, params: IHttpServiceParams = null): Promise<T> | Promise<any>
    {
        return this.request<T>(HttpService.METHOD_POST, url, data, params);
    }


    public request<T>(method: 'GET' | 'POST', url: string, data?, params?: IHttpServiceParamsJson): Promise<T>;
    public request<T>(method: 'GET' | 'POST', url: string, data?, params?: IHttpServiceParamsHtml): Promise<string>;
    public request<T>(method: any, url: any, data: any, params: IHttpServiceParams): Promise<any>;
    public request<T>(method: any, url: any, data: any = null, params: any = null): Promise<any>
    {
        const requestInit: RequestInit = {
            method: method,
        };

        for (const [key, value] of Object.entries(this._defaultRequestInit)) {
            requestInit[key] = value;
        }

        if (params) {
            for (const [key, value] of Object.entries(params)) {
                requestInit[key] = value;
            }
        }
        return fetch(url, requestInit)
            .then(response =>
            {
                if (response.status == 200) {
                    // если необходимо разные типы
                    switch (params.dataType) {
                        case HttpService.DATATYPE_HTML:
                            return response.text();

                        case HttpService.DATATYPE_JSON:
                            return response.json();

                        default:
                            throw Error('unknown dataType');
                    }

                }
                throw Error(`code ${response.status}`);
            });
    }
}